using CompCreate
using MS_Import
using JLD



# Import the chromatogram

pathin="/path/to/the/mxXML/file"
filenames=["filenames.mzXML"]
mz_thresh=[0,600] # the imorted mass range. [0,0] means the full range.

# Parameters for CompCreate


path2features="/path/to/the/feature_list.csv"
mass_win_per=0.8
ret_win_per=0.5
r_thresh=0.8
delta_mass=0.004
min_int=300 # Not needed for comp_ms1()




chrom=import_files(pathin,filenames,mz_thresh)


## For only MS1
comp_list = comp_ms1(chrom,path2features,mass_win_per,ret_win_per,r_tresh,delta_mass)


## For DIA
comp_list = comp_DIA(chrom,path2features,mass_win_per,ret_win_per,r_tresh,delta_mass,min_int)
